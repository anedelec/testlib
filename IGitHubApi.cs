using Refit;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public interface IGitHubApi
{
    [Get("/users/{user}")]
    Task<User> GetUser(string user);
}

public class User
{
}