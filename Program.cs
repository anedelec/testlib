﻿using Refit;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Test_lib
{
    class Program
    {
        static async void MainAsync(string[] args)
        {
            Console.WriteLine("Hello World!");

            var gitHubApi = RestService.For<IGitHubApi>("https://api.github.com");
            var octocat = await gitHubApi.GetUser("octocat");

            Console.ReadLine();
        }
    }
}
